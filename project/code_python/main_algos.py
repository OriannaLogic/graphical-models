from numpy.linalg import norm

def prox_lasso(beta, l_l1, t=1.):
    """Proximal operator for the lasso at beta"""
    beta_abs = np.abs(beta)
    prox_l1 = np.sign(beta) * (beta_abs - t * l_l1) * (beta_abs > t * l_l1)
    return prox_l1

def coord_descent(s12, W11, l_l1=0., n_iter=20):
    p = shape(W11)[0] + 1
    beta0 = np.zeros((p - 1))
    beta = beta0
    beta_new = beta.copy()

    for k in range(0, n_iter):
        beta[:] = beta_new

        # Cycle over the coordinates
        for j in range(0, p - 1):
            uj = s12[j]; W11_j = W11[:,j];
            rj = uj - W11_j.T.dot(beta_new) + W11_j[j] * beta_new[j]
            beta_new[j] = prox_lasso(rj, l_l1) / W11_j[j]

    return beta_new

def permute(M):
    M = roll(M, -1, axis=0)
    M = roll(M, -1, axis=1)
    return M

def graphical_lasso(S, l_l1, n_iter0=30):
    beta = np.empty((p,p))
    diagTheta = np.empty((p))

    def calculate_dist(W_new, W, t=0.001):
        S1 = S.copy()
        np.fill_diagonal(S1, 0)
        return sum(np.absolute(W_new) - np.absolute(W)) - t * sum(S1)

    W = l_l1 * numpy.identity(p) + S
    W_new = W.copy()

    print "Lauching lasso solver..."
    print ' | '.join([name.center(8) for name in ["n_iter", "dist"]])
    dists = []

    n_iter = 0
    not_converged = True
    while(not_converged):
        n_iter += 1
        W_new[:,:] = W

        for j in range(0, p):

            W_new = permute(W_new)
            S = permute(S)

            W11 = W_new[:p-1, :p-1]
            s12 = S[:p-1, p-1]

            beta[:p-1,j] = coord_descent(s12, W11, l_l1)

            w12 = W11.dot(beta[:p-1,j])
            W_new[:p-1, p-1] = w12
            W_new[p-1, :p-1] = w12

        dist = calculate_dist(W_new, W)
        dists.append(dist)

        print ' | '.join( [ ("%d" % n_iter).rjust(8), ("%.2e" % dist).rjust(8)])

        if n_iter > n_iter0:
            break

        not_converged = dist > 0
        W[:,:] = W_new

    for j in range(0, p):
        W_new = permute(W_new)
        diagTheta[j] = 1 / (W_new[p-1, p-1] - W11.dot(beta[:p-1,j]).T.dot(beta[:p-1,j]))
        beta[:, j] = beta[:, j] * (- diagTheta[j])
        beta[p-1, j] = diagTheta[j]
        beta[:, j] = roll(beta[:, j], j + 1)

    for j in range(0, p):
        for i in range(0,j):
            if (beta[i, j] < max(beta[i, j], beta[j, i])):
                beta[i, j] = beta[j, i]
            else: beta[j, i] = beta[i, j]


    return beta