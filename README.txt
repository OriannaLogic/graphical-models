Date: October 2014 to January 2015

This repository contains all the code for the different assignements from the graphical models course of ENS Cachan. 
All the algorithms have been fully recoded in R.
It is not in any case an optimal code that you can find in the available libraries.

The web page of this course can be found on http://www.di.ens.fr/~fbach/courses/fall2014/

Homework 1:
The different types of linear classification (LDA, QDA, logistic regression and Linear regression)

Homework 2:
K-means and Gaussian mixtures (three cases with covariance matrices being proportional to identity - diagonal matrices - general matrices)

Homework 3:
Implementation of the HMM with a four states chain
