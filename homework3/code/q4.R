
##### Initialization ######
train_data_path = "/home/lucas/telecom/mx/graphical_models/homework3/data/EMGaussian.data"
train_data = read.csv(train_data_path, sep='', header=FALSE)

# N° of samples
t = length(train_data$V1)

# Transition matrix
A = (1 / 2 - 1 / 6) * diag(4) + matrix(1 / 6, 4, 4);

# N° of states
K = dim(A)[1]

params = data.frame(
  pis=double(K),
  centers=double(K),
  variances=double(K)
)

sigma1 = matrix(c(2.685313, 0.112865, 0.112865, 3.079135), nrow=2, byrow=TRUE)
sigma2 = matrix(c(7.097200, 5.520687, 5.520687, 9.384972), nrow=2, byrow=TRUE)
sigma3 = matrix(c(0.9494221, -0.2472450, -0.2472450, 6.7010352), nrow=2, byrow=TRUE)
sigma4 = matrix(c(0.1773056, 0.1171894, 0.1171894, 8.1570016), nrow=2, byrow=TRUE)

mu1 = c(-2.113783, 4.244554)
mu2 = c(-3.182797, -3.554309)
mu3 = c(3.732764, -2.752730)
mu4 = c(3.999832, 4.979718)

params$pis = numeric(4) + 1 / K
params$variances = list(sigma1, sigma2, sigma3, sigma4)
params$centers = list(mu1, mu2, mu3, mu4)

EM <- function (data, params_init, A, max_iter = 5, ptest_data = numeric(0)) {
  N = length(data[,1])
  p = length(data[1,])
  iter = 0       
  eps = 1
  
  assign("params", params_init)
  centers = params$centers
  variances = params$variances
  pis = params$pis
  A = A
  
  lls = numeric(max_iter + 1)
  lls[1] = -Inf
  
  test_lls = numeric(max_iter + 1)
  test_lls[1] = -Inf
  
  ll_before = -Inf
  diff_ll = -Inf
  
  while(iter < max_iter) {
    print(diff_ll)
    print(lls)
    print(test_lls)
    if (diff_ll > -Inf) {
      if (abs(diff_ll) < eps) {
        break  
      } 
    }
    iter = iter + 1    
    
    ###### E-STEP ######
    fb <- forward_backward(data, params, A)
    
    if (length(ptest_data) != 0) {
      test_lls[iter + 1] = forward_backward(ptest_data, params, A)$loglikelyhood
    }
    
    pQ <- fb$pQ
    pQQ1 <- fb$pQQ1
    
    # loglikelyhood
    lls[iter + 1] <- fb$loglikelyhood
    
    # Compute the likelyhood difference
    diff_ll <- abs(lls[iter + 1] - ll_before)
    ll_before <- lls[iter + 1]
    
    
    ###### M-STEP ######  
    
    # Maximization of ll with respect to pi
    pis <- pQ[1,] 
    
    # Maximization of ll with respect to transition matrix
    for(i in 1:K){
      for(j in 1:K){
        A[i,j] = sum(pQQ1[,(i-1) * K + j]) / sum(pQ[1:(N-1),i])
      }
    }
    
    # Maximization of ll with respect to the centers
    for (i in 1:K) {
      tmp <- matrix(0, 1, p)
      for (t in 1:N) {
        tmp <- tmp + pQ[t,i] * data[t,]
      }
      centers[[i]] <- tmp / sum(pQ[,i])
      
      # Maximization of ll with respect to the covariance matrices
      ss <- matrix(0, p, p)
      ot <- apply(data, 1, function(x) x - as.matrix(centers[[i]]))
      for (t in 1:N) {
        ss <- ss +  pQ[t,i] * ot[,t] %*% t(ot[,t])
      } 
      variances[[i]] <- ss / sum(pQ[,i])
    }
    
    params$centers = centers
    params$variances = variances
    params$pis = pis 
    
    print(paste("Iter: ", iter))
  }
  
  
  print(lls)
  print(test_lls)
  return(list("pQ"=pQ, "params" = params, "A" = A, "lls"=lls, "test_lls"=test_lls))
}


##### LEARN THE MODEL #####
n = 7

EM1 = EM(train_data, params, A, n - 1)
lls_train <- EM1$lls

mat_prob = cbind(seq(n), lls_train[1:n], 1)
df_ll <- data.frame(iterations = mat_prob[,1], log_likelihood = mat_prob[,2])
df_ll$class[1:n] = "training"
df_ll$class = format(df_ll$class)

graph1 = ggplot(df_ll, aes(iterations, log_likelihood)) + xlim(0,n) + ggtitle("Log likelihoods train set")
graph1 = graph1 + geom_line(aes(colour=class)) + geom_point()
graph1
